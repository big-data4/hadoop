import os
import sys
from paramiko import SSHClient, AutoAddPolicy, RSAKey
from paramiko.auth_handler import AuthenticationException, SSHException
from scp import SCPClient, SCPException
import datetime

import boto3
from config import (host, user, password, remote_path)

sqs = boto3.resource("sqs")
s3 = boto3.resource("s3")
request_queue = sqs.get_queue_by_name(QueueName='big-data-request')
bucket = s3.Bucket('big-data-bucket')
class RemoteClient:

    def __init__(self):
        self.host = host
        self.user = user
        self.password = password
        self.remote_path = remote_path
        self.client = None
        self.scp = None
        self.conn = None

    def _connect(self):
        """Open connection to remote host."""
        if self.conn is None:
            try:
                self.client = SSHClient()
                self.client.set_missing_host_key_policy(AutoAddPolicy())
                self.client.connect(
                    self.host,
                    username=self.user,
                    password=self.password,
                    port=2222
                )
                self.scp = SCPClient(self.client.get_transport())
            except AuthenticationException as error:
                print(f'Authentication failed: \
                    did you remember to create an SSH key? {error}')
                raise error
        return self.client

    def disconnect(self):
        """Close ssh connection."""
        if self.client:
            self.client.close()
        if self.scp:
            self.scp.close()

    def execute_commands(self, commands):
        """
        Execute multiple commands in succession.

        :param commands: List of unix commands as strings.
        :type commands: List[str]
        """
        self.conn = self._connect()
        stdin, stdout, stderr = self.client.exec_command(commands)
        for line in stdout.read().splitlines():
            print (line)

    def download_file(self, file):
        """Download file from remote host."""
        if self.conn is None:
            self.conn = self._connect()
        self.scp.get(file)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit("No filename provided")
    filename = sys.argv[1]
    datestr = datetime.datetime.now().strftime('%d-%m_%H:%M')
    print("Connecting to Hadoop")
    remote = RemoteClient()
    print("Downloading file")
    remote.execute_commands('hadoop fsadmin -get ' + remote_path + '/' + filename + ' ./')
    remote.download_file('./' + filename)
    remote.execute_commands('rm ./' + filename)
    print("Uploading to bucket...")
    bucket.upload_file('./' + filename, datestr + filename)
    request_queue.send_message(MessageBody=datestr + filename)
    os.remove('./' + filename)
