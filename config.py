from os import path, getenv
from dotenv import load_dotenv

# Load environment variables from .env
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

host = getenv('HOST')
user = getenv('REMOTE_USERNAME')
password = getenv('PASSWORD')
remote_path = getenv('REMOTE_PATH')
